import { api } from "boot/axios";

export function apiGetUser() {
  return api({
    url: "/users",
    method: "GET",
  });
}
