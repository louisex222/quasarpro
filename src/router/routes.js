const routes = [
  {
    path: "",
    component: () => import("layouts/MainLayout.vue"),
    meta: { requiresAuth: true },
    redirect: "/index",
    children: [
      {
        path: "index",
        component: () => import("@pages/IndexPage.vue"),
      },

      { path: "snake", component: () => import("@pages/Snake.vue") },
      {
        path: "flappy-bird",
        component: () => import("@pages/FlappyBird.vue"),
      },
      {
        path: "mall-center",
        component: () => import("@pages/MallCenter/MallCenter.vue"),
      },
      { path: "text", component: () => import("@pages/Text.vue") },
      { path: "circle-fork", component: () => import("@pages/CircleFork.vue") },
      {
        path: "content",
        component: () => import("@pages/Content/Content.vue"),
      },
    ],
  },
  {
    path: "",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "docs/tree",
        name: "tree",
        component: () => import("@pages/Docs/Tree.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("@pages/ErrorNotFound.vue"),
  },
  // 登入頁
  {
    path: "/login",
    component: () => import("@pages/LoginPage.vue"),
  },
];

export default routes;
