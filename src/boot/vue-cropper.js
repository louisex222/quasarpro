import { boot } from "quasar/wrappers";
import { VueCropper } from "vue-cropper";

export default boot(({ app }) => {
  console.log("boot vue-cropper");
  app.component("vue-cropper", VueCropper);
});
