const mutations = {
  fcSetRoutes(state, routes) {
    state.route = routes;
  },
  fcGetRoutes(state) {
    return state.routes;
  },

  fcAddRoutes(state, route) {
    if (state.route.indexOf(route) == -1) {
      state.route.push(route);
    }
  },
  fcRemoveRoutes(state, route) {
    state.route.splice(route, 1);
  },
};
export default mutations;
