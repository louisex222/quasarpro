const actions = {
    fcSetRoutes(context, route) {
        context.commit("fcSetRoutes", route);
    },
    fcGetRoutes(context, route) {
        context.commit("fcGetRoutes", route);
    },
    fcAddRoutes(context, route) {
        context.commit("fcAddRoutes", route);
    },
    fcRemoveRoutes(context, route) {
        context.commit("fcRemoveRoutes", route);
    }
}
export default actions
