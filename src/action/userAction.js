import { apiGetUser } from "src/api/userApi";

export const userAction = {
  async getUser() {
    try {
      const response = await apiGetUser();
      return response.data;
    } catch (err) {
      console.log(err);
    }
  },
};
//
